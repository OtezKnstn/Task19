package com.example.task19

import android.app.Activity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.example.task19.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val linearLayoutManager = LinearLayoutManager(this)
        binding.recView.layoutManager = linearLayoutManager


            Thread {
                while (true) {
                try {
                    Thread.sleep(2000)
                } catch (e: InterruptedException) {
                }
                (this as Activity).runOnUiThread {
                    mainWork()
                    Log.d("WORKING", "WORKING WAS DONE SUCCESSFULLY")
                }
                }
            }.start()

    }

    private fun mainWork() {
        val mRequest:WorkRequest = OneTimeWorkRequestBuilder<MyWorker>().build()
        WorkManager.getInstance(this).enqueue(mRequest)
        binding.showData.setOnClickListener {
            binding.recView.adapter = CurrentAdapter.mAdapter
        }
    }
}