package com.example.task19

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.task18.MyDataItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyService: Service() {
    val TAG = "MyService"
    val BASE_URL = "https://jsonplaceholder.typicode.com/"
    var myAdapter:MyAdapter? = null
    init {
        Log.d(TAG, "Service is running...")
    }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        getMyData()
        return START_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent?) {

    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(APIInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>?,
                response: Response<List<MyDataItem>?>?
            ) {
                val responseBody = response?.body()!!

                myAdapter = MyAdapter(responseBody)
                myAdapter?.notifyDataSetChanged()


            }

            override fun onFailure(call: Call<List<MyDataItem>?>?, t: Throwable?) {

            }
        })
    }


}